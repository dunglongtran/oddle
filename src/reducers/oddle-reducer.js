/**
 * Created by quocdung on 5/19/17.
 */
"use strict";
import {
  TAG_USERS_SEARCHING,
  TAG_LIST_HOME_USERS,
  TAG_LIST_FOLLOWERS,
  TAG_CURRENT_NAME_SEARCH,
  TAG_PROFILE,
  TAG_LIST_REPOS,
  TAG_LIST_FOLLOWING

} from '../commons/oddle-tags';
import {
  ACTION_SEARCH_BY_NAME_ERROR,
  ACTION_SEARCH_BY_NAME_LOADING,
  ACTION_SEARCH_BY_NAME_COMPLETE,
  ACTION_PROFILE_LOADING,
  ACTION_PROFILE_COMPLETE,
  ACTION_PROFILE_ERROR,
  ACTION_LIST_FOLLOWING_COMPLETE,
  ACTION_LIST_REPOS_COMPLETE,
  ACTION_LIST_FOLLOWERS_COMPLETE
} from '../commons/oddle-event-type';

const initialState = {
  [TAG_CURRENT_NAME_SEARCH]: '',
  [TAG_USERS_SEARCHING]: false,
  [TAG_LIST_HOME_USERS]: [],
  [TAG_LIST_FOLLOWERS]: [],
  [TAG_LIST_FOLLOWING]: [],
  [TAG_LIST_REPOS]: [],
  [TAG_PROFILE]: null,
};
const handleSearching = (state, action) => {

  return ({...state, [TAG_USERS_SEARCHING]: true, [TAG_CURRENT_NAME_SEARCH]: action.data})
}
const handleDataByName = (state, action) => {
  return ({
    ...state,
    [TAG_LIST_HOME_USERS]: action.data.data,
    [TAG_USERS_SEARCHING]: false,
    [TAG_CURRENT_NAME_SEARCH]: action.data
  })
}
const handleProfile = (state, action) => {
  return ({
    ...state,
    [TAG_PROFILE]: action.data,
  })
};
const handleProfileLoading = (state, action) => {
  return ({
    ...state,
    [TAG_PROFILE]: action.data,
  })
}
const handleRepos = (state, action) => {
  return ({
    ...state,
    [TAG_LIST_REPOS]: action.data,
  })
};
const handleFollower = (state, action) => {
  return ({
    ...state,
    [TAG_LIST_FOLLOWERS]: action.data,
  })
};
const handleFollowing = (state, action) => {
  return ({
    ...state,
    [TAG_LIST_FOLLOWING]: action.data,
  })
};

const oddleReducer = (state = initialState, action) => {
  switch (action.type) {
    case ACTION_SEARCH_BY_NAME_LOADING:
      return handleSearching(state, action);
    case ACTION_SEARCH_BY_NAME_COMPLETE:
      return handleDataByName(state, action);
    case ACTION_PROFILE_LOADING:
      return handleProfile(state, action)
    case ACTION_PROFILE_COMPLETE:
      return handleProfile(state, action);
    case ACTION_LIST_FOLLOWERS_COMPLETE:
      return handleFollower(state, action);
    case ACTION_LIST_FOLLOWING_COMPLETE:
      return handleFollowing(state, action);
    case ACTION_LIST_REPOS_COMPLETE:
      return handleRepos(state, action);
    default:
      return state;
  }
};

export default oddleReducer;
