/**
 * Created by quocdung on 7/15/17.
 */
import React, {Component} from 'react';
import {Route, Link} from 'react-router-dom';
import SearchPage from './containers/SearchPage';
import ProfilePage from './containers/ProfilePage';
import './oddle.scss'

export  default class Oddle extends Component {

  constructor(props) {
    super(props);

  }


  render() {
    return (
      <div>
        <div className="page main-page">
        <Link to="/">
          <h1 className="logo">
            Oddle
          </h1>
        </Link>
        </div>
        <Route exact path='/' component={SearchPage}/>
        <Route path='/profile' component={ProfilePage}/>
      </div>
    )
  }
}


