/**
 * Created by quocdung on 7/15/17.
 */
"use strict";
import React, {Component} from 'react';
import {style} from './style';


export  default class ListUserItem extends Component {

  handleClick(e) {
    const {url} = this.props.data;

    this.props.getProfile(url, this.props.data);
    this.props.history.push('/profile')
  }

  componentWillMount() {

  }

  render() {
    const {avatar_url, login, score, url} = this.props.data;
    return (
      <li className="list-item">
        <div className="item-content" onClick={this.handleClick.bind(this)}>
          <img  src={avatar_url}/>
          <div className="item-info" >
            <h3>{login}</h3>
            <span>{`score: ${score}`}</span>
          </div>
        </div>
      </li>

    );
  }
}
