/**
 * Created by quocdung on 7/15/17.
 */
import React, {Component} from 'react';
import {TAG_LIST_HOME_USERS, TAG_LIST_USERS} from '../../commons/oddle-tags';
import ListUserItem from "./list-item";
import {style} from './style';


export default class ListUser extends Component {

  generateListItem() {
    console.log(this.props)
    let items = (this.props[TAG_LIST_USERS] || this.props[TAG_LIST_HOME_USERS].items) || [];
    return items.map((item) => {
      return (<ListUserItem style={style.listItem} key={item.id} data={item} {...this.props}/>)
    })
  }

  render() {
    return (
      <div className="list-user">
        {(this.props[TAG_LIST_HOME_USERS] && this.props[TAG_LIST_HOME_USERS]['total_count']) ? (
          <h3 className="list-divider">
            {`Results for ${this.props[TAG_LIST_HOME_USERS]['total_count']}`}
          </h3>
        ) : ''
        }
        <ul className="list">
          {this.generateListItem()}
        </ul>
      </div>
    );
  }

}
