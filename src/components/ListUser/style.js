/**
 * Created by quocdung on 7/16/17.
 */
import  {styles} from '../../commons/styles';
"use strict";
export const style = {
  margin: 'auto',

  divider: Object.assign(styles.header, {}),
  list: {display: 'flex', flexWrap: 'wrap', justifyContent: 'space-between', alignItems: 'baseline'},
  listItem: Object.assign(styles.li, {
    display: 'inline-flex',
    maxWidth: 300,
    width:'auto',
    minWidth: 150,
    height: 300,
    margin:'10px',
    flex:1,
  }),
  itemContent: {
    display: 'block',
    width: '100%',
    height: '100%',
    position: 'relative'
  },
  itemInfo:{
    position: 'absolute',
    bottom:0,
    width:'100%'
  }
}

