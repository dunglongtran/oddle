/**
 * Created by quocdung on 7/15/17.
 */
import  React, {Component} from 'react';

import {
  TAG_PROFILE,
  TAG_LIST_FOLLOWERS,
  TAG_LIST_FOLLOWING,
  TAG_LIST_REPOS,
  TAG_LIST_USERS
} from '../../commons/oddle-tags';
import {connect} from 'react-redux';

import {oddleAction} from'../../actions/oddle-action';
import ListUser from '../../components/ListUser'

class ProfilePage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      follower: true,
    }

  }

  componentWillMount() {
    if (!this.props[[TAG_PROFILE]]) {
      this.props.history.replace('/');
      this.props.history.go();
    }
  }

  generateRepos() {
    const repos = this.props[TAG_LIST_REPOS] || [];
    return repos.map((repos) => {
      let {id, name, description, language} = repos;
      return ( <li key={id} className="repos-item">
        <div className="repos-content">
          <h3 className={'name'}>{name}</h3>
          <span className={'description'}>{description}</span>
          <span className={'language'}>{language}</span>
        </div>
      </li>)
    });
  }

  render() {
    let {
      avatar_url, location, name, login,
      public_gists, public_repos,
      followers, following,
      company, bio, created_at,
      updated_at
    } = this.props[TAG_PROFILE];
    const listFollower = {[TAG_LIST_USERS]: this.props[TAG_LIST_FOLLOWERS]};
    const listFollowing = {[TAG_LIST_USERS]: this.props[TAG_LIST_FOLLOWING]};
    return (

      <div className="page profile-page">
        <section className="profile-info">
          <img src={avatar_url}/>
          <div className="info-content">
            <h3> {(name || login) ? (name || login) : ''}</h3>
            <span>{`Location : ${location ? location : 'N/A'}`}</span>
            <span>{`Company : ${company ? company : 'N/A'}`}</span>
            <span>{`Bio : ${bio ? bio : 'N/A'}`}</span>
            <span>{`Created_at : ${created_at ? created_at : 'N/A'}`}</span>
            <span>{`Updated_at : ${updated_at ? updated_at : 'N/A'}`}</span>
          </div>
        </section>
        <section className="profile-group profile-repo">
          <span className="sub-header">{`Repos: ${public_repos}`}</span>
          <div className="group-list">
            <ul className="list-repos">
              {this.generateRepos()}
            </ul>
          </div>
        </section>
        <section className="profile-group profile-follow">
          <div className="tab tab-follow">
            <button onClick={() => this.setState({follower: true})}>Follower</button>
            <button onClick={() => this.setState({follower: false})}> Follow</button>
          </div>
          { !this.state.follower || (
            <div >
              <span className="sub-header">{`Follower: ${followers}`}</span>
              <ListUser  {...listFollower} {...this.props}/>
            </div>
          )
          }
          { this.state.follower || (
            <div >
              <span className="sub-header">{`Following: ${following}`}</span>
              <ListUser  {...listFollowing} {...this.props}/>
            </div>
          )
          }
        </section>
      </div>
    );
  }
}
const mapDispatchToProps = dispatch => {
  return {
    searchByName: (name) => dispatch(oddleAction.searchByName(name)),
    getProfile: (url, item) => dispatch(oddleAction.getProfile(url, item)),
  }
};
const mapStateToProps = state => {
  return {
    [TAG_PROFILE]: state.oddleReducer[TAG_PROFILE],
    [TAG_LIST_FOLLOWERS]: state.oddleReducer[TAG_LIST_FOLLOWERS],
    [TAG_LIST_FOLLOWING]: state.oddleReducer[TAG_LIST_FOLLOWING],
    [TAG_LIST_REPOS]: state.oddleReducer[TAG_LIST_REPOS],

  }
};
export default connect(mapStateToProps, mapDispatchToProps)(ProfilePage);
