/**
 * Created by quocdung on 7/15/17.
 */
import  React, {Component} from 'react';
import {style} from './style';

import {TAG_LIST_HOME_USERS, TAG_CURRENT_NAME_SEARCH, TAG_USERS_SEARCHING} from '../../commons/oddle-tags';
import {connect} from 'react-redux';

import {oddleAction} from'../../actions/oddle-action';
import ListUser from '../../components/ListUser'


class SearchPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: ''
    }
  }

  render() {

    console.log(this.props[TAG_CURRENT_NAME_SEARCH])
    return (
      <div className="page ">
        <div className="search-bar">
          <input className="search-box" type="text" value={this.state.name}
                 placeholder="Enter user name to search"

                 onChange={(e) => this.setState({name: e.currentTarget.value})}/>
          <button className="search-button"
                  onClick={(e) => {
                    this.props.searchByName(this.state.name)
                  }}>Search
          </button>
        </div>

        {!this.props[TAG_USERS_SEARCHING] || (
          <span className="sub-header">
            { `Searching by ${this.props[TAG_CURRENT_NAME_SEARCH]} ...`}
          </span>
        )}
        { !this.props[TAG_LIST_HOME_USERS] || (
          <ListUser {...this.props}/>
        )

        }

      </div>
    );
  }
}
const mapDispatchToProps = dispatch => {
  return {
    searchByName: (name) => dispatch(oddleAction.searchByName(name)),
    getProfile: (url, data) => dispatch(oddleAction.getProfile(url, data)),
  }
}

const mapStateToProps = state => {
  return {
    [TAG_LIST_HOME_USERS]: state.oddleReducer[TAG_LIST_HOME_USERS],
    [TAG_USERS_SEARCHING]: state.oddleReducer[TAG_USERS_SEARCHING],
    [TAG_CURRENT_NAME_SEARCH]: state.oddleReducer[TAG_CURRENT_NAME_SEARCH],
  }
    ;
}
export default connect(mapStateToProps, mapDispatchToProps)(SearchPage);
