/**
 * Created by quocdung on 7/15/17.
 */
import {styles} from '../../commons/styles'

const pageSearch = {
  searchBar: {
    display: 'flex',
    justifyContent: 'center',
  },
  searchBox: {
    fontSize: 20,
    maxWidth: 600,
    flex: 'auto',
  },
  searchBtn: {
    color: 'rgb(189,11,0)',
    fontSize: 20,
    margin: 'auto 12px',
  }
}
export const style = Object.assign(styles.page, pageSearch);
