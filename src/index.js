import React from 'react';
import ReactDOM from 'react-dom';
//
import {Router, browserHistory,IndexRoute} from 'react-router';
import {HashRouter, BrowserRouter, Route, Switch} from 'react-router-dom';

import createBrowserHistory from 'history/createBrowserHistory';

import {Provider} from 'react-redux';
import {createStore} from 'redux';

import reducerApp from './reducers';

import store from "./store";


import './index.scss';
import  Oddle from './Oddle';


const history = createBrowserHistory();

ReactDOM.render(
  <Provider store={store}>
    <BrowserRouter>
      <Oddle/>
    </BrowserRouter>
  </Provider>
  ,
  document.getElementById('root')
);
