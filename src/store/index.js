/**
 * Created by quocdung on 5/20/17.
 */
import {createStore, applyMiddleware, compose} from "redux";
import thunk from "redux-thunk";
import reducers from "../reducers/";
// import {syncHistoryWithStore, routerReducer} from 'react-router-redux';
const middleware = [thunk];
export default compose(
    applyMiddleware(...middleware)
)(createStore)(reducers);
