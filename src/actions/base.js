/**
 * Created by quocdung on 5/19/17.
 */
class BaseAction {
    sendStatus(type) {
        return {type};
    }

    sendStatusWithData(type, data) {
        return {type, data};
    }

}

export default BaseAction