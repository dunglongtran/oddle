/**
 * Created by quocdung on 7/15/17.
 */
"use strict";

import BaseAction from './base';
import {
  ACTION_PROFILE_COMPLETE,
  ACTION_PROFILE_LOADING,
  ACTION_PROFILE_ERROR,
  ACTION_LIST_FOLLOWERS_COMPLETE,
  ACTION_LIST_FOLLOWERS_ERROR,
  ACTION_LIST_FOLLOWERS_LOADING,
  ACTION_SEARCH_BY_NAME_COMPLETE,
  ACTION_SEARCH_BY_NAME_ERROR,
  ACTION_SEARCH_BY_NAME_LOADING,
  ACTION_LIST_FOLLOWING_COMPLETE,
  ACTION_LIST_FOLLOWING_LOADING,
  ACTION_LIST_FOLLOWING_ERROR,
  ACTION_LIST_REPOS_COMPLETE,
  ACTION_LIST_REPOS_LOADING,
  ACTION_LIST_REPOS_ERROR,
} from '../commons/oddle-event-type'
class OddleAction extends BaseAction {
  searchByName(name) {
    return dispatch => {
      dispatch(this.sendStatusWithData(ACTION_SEARCH_BY_NAME_LOADING, name));
      getListUsersByName(dispatch, this)(name);
    }
  }

  getListFollowes(url) {
    return dispatch => {
      dispatch(this.sendStatusWithData());

    }
  }

  getProfile(url, item) {
    return dispatch => {
      dispatch(this.sendStatusWithData(ACTION_PROFILE_LOADING, item));
      getProfile(dispatch, this)(url);
      getFollower(dispatch, this)(item.followers_url);
      getFollowing(dispatch, this)(`https://api.github.com/users/${item.login}/following`);
      getRepos(dispatch, this)(item.repos_url);
    }
  }

  getListRepos(name) {
    return dispatch => {
      dispatch(this.sendStatusWithData());
    }
  }

}
;

export const oddleAction = new OddleAction();

const getListUsersByName = (dispatch, action) => (name) => {
  fetch(`https://api.github.com/search/users?q=${name}`).then(res => {
    if (!res.ok) {
      throw new Error(`Github not found by ${name}`);
    }
    return res.json();
  }).then(json => {
    dispatch(action.sendStatusWithData(ACTION_SEARCH_BY_NAME_COMPLETE, {'name': name, 'data': json}));
  }).catch(error => {
    dispatch(action.sendStatusWithData(ACTION_SEARCH_BY_NAME_ERROR, error));
  })
};
const getProfile = (dispatch, action) => (url) => {
  fetch(url).then(res => {
    if (!res.ok) {
      throw new Error(`Can't get profile from GitHub`);
    }
    return res.json();
  }).then(json => {
    dispatch(action.sendStatusWithData(ACTION_PROFILE_COMPLETE, json));
  }).catch(error => {
    dispatch(action.sendStatusWithData(ACTION_PROFILE_ERROR, error));
  })
};
const getRepos = (dispatch, action) => (url) => {
  fetch(url).then(res => {
    if (!res.ok) {
      throw new Error(`Can't get repos from GitHub`);
    }
    return res.json();
  }).then(json => {
    dispatch(action.sendStatusWithData(ACTION_LIST_REPOS_COMPLETE, json));
  }).catch(error => {
    dispatch(action.sendStatusWithData(ACTION_LIST_REPOS_ERROR, error));
  })
};
const getFollower = (dispatch, action) => (url) => {
  fetch(url).then(res => {
    if (!res.ok) {
      throw new Error(`Can't get follower from GitHub`);
    }
    return res.json();
  }).then(json => {
    dispatch(action.sendStatusWithData(ACTION_LIST_FOLLOWERS_COMPLETE, json));
  }).catch(error => {
    dispatch(action.sendStatusWithData(ACTION_LIST_FOLLOWERS_ERROR, error));
  })
};
const getFollowing = (dispatch, action) => (url) => {
  fetch(url).then(res => {
    if (!res.ok) {
      throw new Error(`Can't get follower from GitHub`);
    }
    return res.json();
  }).then(json => {
    dispatch(action.sendStatusWithData(ACTION_LIST_FOLLOWING_COMPLETE, json));
  }).catch(error => {
    dispatch(action.sendStatusWithData(ACTION_LIST_FOLLOWING_ERROR, error));
  })
};
